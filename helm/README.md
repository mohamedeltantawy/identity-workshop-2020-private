
# Running in Kubernetes

## Prerequisites
All of the following tools are required to be able to run locally the provided helm charts. If you are installing them for the first time, make sure you are following the order below:

* helmfile
* helm
* helm-diff
* k9s
* docker
* docker-for-desktop
* local k8s cluster enabled

- macOS / Linux - whilst everything should also run on Windows, currently this README.md only provides steps for bash users.

## Initial steps

### Install helmfile
```bash
$ brew install helmfile
```
### Install helm
Helm 2.16.1 tested and supported by backbase

Check helm version
```bash
$ helm version
```
Install helm first time
```bash
$ brew install helm@2
```

> Note: make sure you have the installed helm 2.16.1 version and it's the same version on cluster tiller
as below
```bash
helm version
Client: &version.Version{SemVer:"v2.16.1", GitCommit:"bbdfe5e7803a12bbdf97e94cd847859890cf4050", GitTreeState:"clean"}
Server: &version.Version{SemVer:"v2.16.1", GitCommit:"bbdfe5e7803a12bbdf97e94cd847859890cf4050", GitTreeState:"clean"}
```

if you faced an issue with helm versions use this command
```bash
$ curl -L https://git.io/get_helm.sh | bash -s -- --version v2.16.1
```

### Install helm-diff
```bash
$ helm plugin install https://github.com/databus23/helm-diff --version master
```
### Install k9s
```bash
$ xcode-select --install
$ brew install derailed/k9s/k9s
```

### Install docker / docker-desktop
Install from https://www.docker.com/products/docker-desktop \
Enable Kubernetes from `Preferences -> Kubernetes -> Enable Kubernetes`

Increase memory used by Docker 25 GB

`Preferences -> Advanced -> Memory `
Or 
`Preferences -> Resoureces -> Advanced -> Memory `


 
## Setup
### 1. Setup helm
```bash
$ helm init
$ helm install stable/nginx-ingress --name ingress
$ sh backbase-repo-add.sh
```

### 2. Add records to /etc/hosts
```bash
$ sudo vi /etc/hosts
```
Add:
```
127.0.0.1 backbase.local
127.0.0.1 registry.backbase.local
```

### 3. Build docker images for IPS, Identity and CXS
From the root folder
```bash
$ mvn clean install -Pdocker

```
### 4. Start k9s
You can start k9s to monitor the deployment
```bash
$ k9s
```

### 5. Deploy BB services
From `helm` root folder
```bash
$ cd helm/
$ helmfile -l "tier=infrastructure" apply
$ helmfile -l "tier=ips" apply
$ helmfile -l "tier=identity" apply
$ helmfile -l "tier=cxs" apply
```

Optionally, Access Control can also be deployed with identity integration:
```bash
$ helmfile -l "tier=dbs,capability=access-control" apply
```

All other DBS capabilities can be deployed without any additional identity configuration, for example: 
```bash
$ helmfile -l "tier=dbs,capability=product-summary" apply
```

### 6. Import Experience Manager
From the root folder
```bash
cd statics

mvn package bb:provision -Dportal.host=backbase.local -Dportal.port=80 -Dcontextpath=/api -Didentity.url=http://backbase.local 
mvn bb:import-experiences -Dportal.host=backbase.local -Dportal.port=80 -Dcontextpath=/api -Didentity.url=http://backbase.local
mvn bb:import-packages -Dportal.host=backbase.local -Dportal.port=80 -Dcontextpath=/api -Didentity.url=http://backbase.local   

```
From the root folder run the below command to provision bb providers 
```bash
cd collection-ku8s-project
mvn package bb:provision -Dportal.host=backbase.local -Dportal.port=80 -Dcontextpath=/api -Didentity.url=http://backbase.local
```
### 7. (Optional) Reconfigure Experience Manger to authenticate against Identity
If CXS has been deployed with the IDENTITY flag set, this isn't required as portal should default to redirecting users to identity.

From the root folder
```bash
$ cd experience-config/
$ ./build.sh
```

You should now be able to access CXP Manager (http://backbase.local/cxp-manager/login) and authenticate through Identity (admin/admin).

You should now be able to access the Identity Admin Console (http://backbase.local/auth) and can login (admin/admin).


## Open the registry web dashboard
```bash
http://registry.backbase.local/
```


## To run ingestion
runt the below commands to port-forward integration services then configure teh environment variable in Postman collection to use the new ports
```bash
kubectl port-forward svc/dbs-access-control-legalentityintegrationservice  8181:8080
kubectl port-forward svc/dbs-access-control-userintegrationservice  8182:8080
kubectl port-forward svc/dbs-product-summary-arrangementsintegrationservice 8183:8080
kubectl port-forward svc/dbs-access-control-accessgroupintegrationservice  8184:8080
kubectl port-forward svc/identity-oidctokenconverter  8280:8080 
kubectl port-forward svc/identity-fidoservice  8281:8080 
```