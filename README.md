# Identity Workshop (2020)

**Identity and Access Management** is a framework of policies and technologies for ensuring that the proper people in an enterprise have the appropriate access to technology resources.

For the purpose of this exercise please clone this repository. Project contains all the artifacts necessary to bootstrap and run Identity Server Setup.

### Cloning the repo

Clone this repo into your computer. This is a self contained project with all the necessary artifacts declared as maven dependencies.


### 1. Setup environment 
The below instruction describe two ways to run the environment. in cause of using Kubernetes all URLs and Postman Environment variables might vary you have to adapt themto be able to pick the correct services

**Run with Kubernetes**
 [Backbase 6 :: Helm Instruction](helm/README.md)
> NOTE: It's require experience with Helm and Kubernetes 

Machine Requirements
> Kubernetes Environment requires 25 GB free memory to run local Kubernetes cluster 

**Run with Blade**
 [Backbase 6 :: Blade Instruction](blade-instruction.md)

##### Test all services are running correctly 
    
    | Compoenent        |  URL                                                              |
    | ------            | ------                                                            | 
    | Identity          | http://localhost:8180/auth                                        |
    | CXP Manager       | http://localhost:8080/gateway/cxp-manager/login                   |
    | Retail App        | http://localhost:8080/gateway/retail-banking-demo-wc3/index       |
    | Bank Employee App | http://localhost:8080/gateway/bank-employee-demo/login            |
    

> Note: user [admin/admin] credentials to login to all the services above

> Note: Import this package to your cxp-manager enterprise catalog statics/00001_theme-bb-backbase.zip

### 2. Create Users in Identity
        2.1 Create Sara User
            - From Identity Console [Users -> Add user]
                - Username      -> sara
                - First Name    -> sara
                - save
        2.2 Set Password 
            - From Identity Console [Users] -> View all users -> edit sara user -> Credentials 
                - New Password          -> sara
                - Password Confirmation -> sara
                - Temporary             -> off
                - save
        2.3 Add correct roles
            - From Identity Console [Users] ->  edit sara user -> Role Mappings
                - Select ROLE_group_user(USER) and ROLE_USER to Assigned Roles
                
        2.4 One the retail app and login with [sara]
            - (http://localhost:8080/gateway/retail-banking-demo-wc3/index)

### 3. Configure Password Policy
    3.1 Configure password policy for [Backbase] relem
        - From Identity Console [Authentication -> Password Policy]
            - Add Policy -> Minimume Length -> Enter 6 -> save
    
    3.2 Change sara's password
        - From Identity Console [Users] ->  View all users -> edit sara user -> Credentials 
            - New Password          -> sara
            - Password Confirmation -> sara
            - Temporary             -> off
            - save
                
> Note: You expect error cause this password violate the password policy, then use a proper password match your policy and click save

### 4. Configure Required Actions    
    
    4.1 Configure OTP as a required actions for sara
         - From Identity Console [Users] ->  View all users -> edit sara user 
             - In [Required User Actions] field type [Configure OTP]
             - save
    
    4.2 Test Login with Sara
         - Open [http://localhost:8180/auth/realms/backbase/account]
            - Type sara username/password
            - Open your [Google Authenticator from your mobile] download link
                -  [ANDROID](https://play.google.com/store/apps/details?id=com.google.android.apps.authenticator2&hl=en)
                -  [IOS](https://apps.apple.com/us/app/google-authenticator/id388497605)
            - Scan the barcode and type the OTP
            - Login
        
> NOTE: Every time you will try to login with sara you need to provide a valid OTP code
       

### 5. Setup Bank
    Download the below postman collection and envelopment variables
    5.1 Import the below collections to your postman
        -  [Identity Workshop Postman Collection](postman/Identity-workshop-2020.postman_collection.json)
        -  [Identity Workshop Postman Env](post/identity-workshoup-2020-env.postman_environment.json)
    5.2 And run Setup Bank Collection one by one

> NOTE: This collection will create the Bank and Customer [Legal Entities] , [Service Agreements] and ingest products for Customer [sara]
> NOTE: if you recieved and error `Identity retrieved does not have a valid name.` while importing user from Identity you need to set the `First Name` for that user in Identity cause it's mandatory in DBS 

! [Postman collection](doc/postman-collections.png?raw=true)

### 6. Test Ingested data
    Execute in Postman collection named [Login with Sara] 
        6.1 Execute [Login with Sara]
        6.2 Execute [Get Product Summary]
        
> Note: You expect to retrieve Sara's product summary as one current account

### 7. Add [cid] Custom Claim to External
    Configure Identity mapper to pass user attribute in token claim set
        7.1  From Identity Console go to Users -> View all users -> sara -> Attribute
            - In Key      -> type [cid]
            - In Value    -> type any customer id 
            - Add 
            - Save
            
        7.2  From Identity Console got to Clients -> bb-web-client -> Mappers - create
            - In Mapper Type      -> choose [User Attribute]
            - In User Attribute   -> type attribute name [cid]
            - In Token Claim Name -> type [cid]
            - In Claim JSON Type  -> choose [string]
            - save
            
        7.3 One (http://localhost:8080/gateway/retail-banking-demo-wc3/index) and try to login again then watch the logs
            
> Note: It should look like the below 
 [doc/cid-mapper.png](doc/cid-mapper.png)

### 8. Create Custom Authenticator to validate User attribute [cid] is required
    8.1 Follow the TODOs in Identity module starts with [TODO][Custom Authenticator]]
    
    8.2 Clean and build Identity module with
        - $ mvn clean install -Pdocker
        
    8.3 Stop and Run docker-compose
        - $ docker-compose up
        
    8.4 From Identity Console go to Authentication -> Flows -> From Dropdown Select Browser -> Copy -> ok
    
    8.5 From Identity Console go to Authentication -> Flows -> Bindings
        - Browser Flow -> select [Copy of Browser]
        - save
        
    8.6 From Identity Console go to Authentication -> From Dropdown  select [Copy of Browser]
        - Click [Add execution] -> From Dropdown select [User Attribute Verification (cid)]
        - save
        
    8.7 You can see the new authentication step added as last step in the flowo
        - Make it [Required]
        
    8.8 Go to [http://localhost:8180/auth/realms/backbase/account]
        - try to login with [sara/sara] -> expected to successed 
        - try to login with [admin/admin] -> expected to fail with error message [Missing CID Attribute]
     

### 9. Implement Custom Internal Token Enhancer
    9.1 Follow the TODOs in Identity Modules starts with [TODO][Internal Token]]
    
    9.2 Open postman collection refared eariler and execute login with sara [sara/sara] and watch the logs
    

### 10. Demo Device Registration/Authentication 

