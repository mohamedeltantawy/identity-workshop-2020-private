# Backbase Project Blade Instruction

This project is created by http://start.backbase.com


## Prerequisites
All of the following tools are required to be able to run locally, If you are installing them for the first time, make sure you are following the order below:
* java 8
* maven
* docker

### 1. Build and Run Project locally
> Note: make sure you run the following command in order

Run all the following command from Root folder

##### 1.1 Build and Run Platform
```bash
cd platfrom
mvn clean install -Pdocker
mvn blade:run
```

#####  1.2 Build and Run Identity and MFA services
```bash
cd identity
mvn clean -Pclean-database
mvn clean install -Pdocker
docker-compose up -d
mvn blade:run -Dlogging.level.com.backbase=DEBUG
```

##### 1.3 Build and Run CX6
```bash
cd cx6-targeting
mvn clean -Pclean-database
mvn blade:run
```

##### 1.4 Build and Run Access Control
```bash
cd dbs/access-control
mvn clean -Pclean-database
mvn blade:run
```

##### 1.5 Build and Run Product Summary
```bash
cd dbs/product-summary
mvn clean -Pclean-database
mvn blade:run
```

### 2. Import Statics

##### 2.1 From the root folder
```bash
cd statics
mvn package bb:provision 
mvn bb:import-experiences
mvn bb:import-packages

```

##### 2.2 From the root folder run the below command to provision bb providers 
```bash
cd collection-ku8s-project
mvn package bb:provision
```
### 3. (Optional) Reconfigure Experience Manger to authenticate against Identity
If CXS has been deployed with the IDENTITY flag set, this isn't required as portal should default to redirecting users to identity.

> Note: Use this step if you create new Realm and want to configure a special portal to use the new realm.
 
From the root folder
```bash
$ cd experience-config/
$ ./build.sh
```

You should now be able to access CXP Manager (http://localhost:8080/gateway/cxp-manager/login) and authenticate through Identity (admin/admin).
You should now be able to access the Identity Admin Console (http://localhost:8180/auth) and can login (admin/admin).
Open the registry web dashboard (http://localhost:8080/registry/)

