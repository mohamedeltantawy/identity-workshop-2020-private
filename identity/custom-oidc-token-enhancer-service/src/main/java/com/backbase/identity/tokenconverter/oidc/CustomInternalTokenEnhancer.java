package com.backbase.identity.tokenconverter.oidc;

import com.backbase.buildingblocks.jwt.core.util.JsonWebTokenUtils;
import com.backbase.identity.tokenconverter.enhancer.InternalTokenEnhancer;
import com.nimbusds.jwt.SignedJWT;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import java.text.ParseException;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;


/**
 * @copyright (C) 2020, Backbase
 * @Version 1.0
 * @Since 10. Feb 2020 10:05 AM
 */

@Component
public class CustomInternalTokenEnhancer implements InternalTokenEnhancer {

    private static final Logger logger = LoggerFactory.getLogger(CustomInternalTokenEnhancer.class);
    public static final String CID_CLAIM_NAME = "cid";

    @Autowired
    private HttpServletRequest request;

    @Override
    public OAuth2AccessToken enhance(OAuth2AccessToken accessToken, OAuth2Authentication authentication) {

        // [TODO][Internal Token][1] add the custom claims to the access token by calling getCustomClaims and them to accessToken.getAdditionalInformation.putAll
        accessToken.getAdditionalInformation().putAll(getCustomClaims());
        return accessToken;
    }

    private Map<String, Object> getCustomClaims() {

        Map<String, Object> claims = new HashMap<>();

        //An example of retrieving a custom claim from a external token.
        try {
            String externalToken = null;

            // [TODO][Internal Token][2] read the external token from the request header or the cookies
            /**
             * HINT: Use this code block
             *
             * if (JsonWebTokenUtils.isHeaderBasedAuth(HttpHeaders.AUTHORIZATION, request)) {
             *      logger.info("Getting token from headers");
             *      Optional<String> headerValue = JsonWebTokenUtils.getHeaderValue(HttpHeaders.AUTHORIZATION, request);
             *      externalToken = headerValue.orElseThrow(RuntimeException::new).split(" ")[1];
             *  } else if (JsonWebTokenUtils.isCookieBasedAuth(HttpHeaders.AUTHORIZATION, request)) {
             *      logger.info("Getting token from Cookies");
             *      externalToken = JsonWebTokenUtils.getCookieValue(HttpHeaders.AUTHORIZATION, request).orElseThrow(RuntimeException::new);
             *  }
             */
            if (JsonWebTokenUtils.isHeaderBasedAuth(HttpHeaders.AUTHORIZATION, request)) {
                logger.info("Getting token from headers");
                Optional<String> headerValue = JsonWebTokenUtils.getHeaderValue(HttpHeaders.AUTHORIZATION, request);
                externalToken = headerValue.orElseThrow(RuntimeException::new).split(" ")[1];
            } else if (JsonWebTokenUtils.isCookieBasedAuth(HttpHeaders.AUTHORIZATION, request)) {
                logger.info("Getting token from Cookies");
                externalToken = JsonWebTokenUtils.getCookieValue(HttpHeaders.AUTHORIZATION, request).orElseThrow(RuntimeException::new);
            }

            logger.info("Trying to get customer id from external token");

            String cid = null;

            // [TODO][Internal Token][3] call getCustomerIdFromExternalToken and pass the externalToken and save the value in cid
            cid = getCustomerIdFromExternalToken(externalToken);

            logger.info("CustomerId found: " + cid);

            logger.info("Adding CustomerId to internal token claim set");

            claims.put(CID_CLAIM_NAME, cid);


        } catch (Exception e) {
            logger.error("Error parsing token", e);
        }

        return claims;
    }

    private String getCustomerIdFromExternalToken(String externalToken) throws ParseException {

        String cid = null;

        // [TODO][Internal Token][4] parse the externalToken and retrive the cid from the claims
        /**
         * HINT: use this code block
         * SignedJWT.parse(externalToken).getPayload().toJSONObject().getAsString(CID_CLAIM_NAME);
         *
         */

        cid = SignedJWT.parse(externalToken).getPayload().toJSONObject().getAsString(CID_CLAIM_NAME);

        if (cid == null) {
            logger.error("Could note fined Customer Id in external token");
            return "empty-cid";
        }

        return cid;
    }
}
