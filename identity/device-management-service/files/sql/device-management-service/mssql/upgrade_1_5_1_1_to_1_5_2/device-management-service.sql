-- Changeset src/main/resources/db/changelog/db.changelog-1_5_2.yml::device003::corey
-- Creates an index for users dbs userId
CREATE NONCLUSTERED INDEX ix_dbs_user_id ON [device]([dbs_user_id])
GO

ALTER TABLE [device] ADD [friendly_name] [nvarchar](50)
GO

ALTER TABLE [device] ADD [vendor] [nvarchar](100)
GO

ALTER TABLE [device] ADD [model] [nvarchar](100)
GO

