--  Changeset src/main/resources/db/changelog/db.changelog-1_5_0.yml::fido001::jimd
CREATE TABLE application (id INT AUTO_INCREMENT NOT NULL, app_key VARCHAR(128) NOT NULL, app_id VARCHAR(255) NOT NULL, CONSTRAINT pk_application PRIMARY KEY (id));

ALTER TABLE application ADD CONSTRAINT uq_application_app_key UNIQUE (app_key);

ALTER TABLE application ADD CONSTRAINT uq_application_app_id UNIQUE (app_id);

CREATE TABLE trusted_facet (id INT AUTO_INCREMENT NOT NULL, application_id INT NOT NULL, trusted_facet_id VARCHAR(128) NULL, CONSTRAINT pk_trusted_facet PRIMARY KEY (id));

ALTER TABLE trusted_facet ADD CONSTRAINT fk_trusted_facet_to_app FOREIGN KEY (application_id) REFERENCES application (id) ON UPDATE NO ACTION ON DELETE NO ACTION;

CREATE TABLE registration_request (id INT AUTO_INCREMENT NOT NULL, application_id INT NOT NULL, challenge VARCHAR(86) NULL, device_id VARCHAR(36) NOT NULL, user_id VARCHAR(36) NOT NULL, username VARCHAR(255) NOT NULL, processed BIT(1) NOT NULL, CONSTRAINT pk_registration_request PRIMARY KEY (id));

ALTER TABLE registration_request ADD CONSTRAINT fk_registration_request_to_app FOREIGN KEY (application_id) REFERENCES application (id) ON UPDATE NO ACTION ON DELETE NO ACTION;

CREATE TABLE registration (id INT AUTO_INCREMENT NOT NULL, application_id INT NOT NULL, device_id VARCHAR(36) NOT NULL, user_id VARCHAR(36) NOT NULL, username VARCHAR(255) NOT NULL, aa_id VARCHAR(128) NULL, public_key_base_64_encoded VARCHAR(512) NULL, key_id VARCHAR(128) NULL, signature_counter BIGINT NULL, authenticator_version INT NULL, CONSTRAINT pk_registration PRIMARY KEY (id));

ALTER TABLE registration ADD CONSTRAINT fk_registration_to_app FOREIGN KEY (application_id) REFERENCES application (id) ON UPDATE NO ACTION ON DELETE NO ACTION;

CREATE TABLE authentication_request (id INT AUTO_INCREMENT NOT NULL, application_id INT NOT NULL, challenge VARCHAR(86) NULL, user_id VARCHAR(36) NOT NULL, username VARCHAR(255) NOT NULL, processed BIT(1) NOT NULL, CONSTRAINT pk_authentication_request PRIMARY KEY (id));

ALTER TABLE authentication_request ADD CONSTRAINT fk_authen_request_to_app FOREIGN KEY (application_id) REFERENCES application (id) ON UPDATE NO ACTION ON DELETE NO ACTION;

