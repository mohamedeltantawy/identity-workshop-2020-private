package com.backbase.identity.custom.authenticator;

import org.jboss.logging.Logger;
import org.keycloak.authentication.AbstractFormAuthenticator;
import org.keycloak.authentication.AuthenticationFlowContext;
import org.keycloak.authentication.AuthenticationFlowError;
import org.keycloak.models.KeycloakSession;
import org.keycloak.models.RealmModel;
import org.keycloak.models.UserModel;

import javax.ws.rs.core.Response;

/**
 * @copyright (C) 2019, Backbase
 * @Version 1.0
 * @Since 16. Dec 2019 7:03 PM
 */
public class UserAttributeVerificationAuthenticator extends AbstractFormAuthenticator {

    public static final String CID_ATTRIBUTE_NAME = "cid";
    public static final String INVALID_CID_ERROR_MESSAGE = "Missing `cid` Attribute";

    private static final Logger logger = Logger.getLogger(UserAttributeVerificationAuthenticator.class);

    @Override
    public void action(AuthenticationFlowContext context) {
        // [TODO][Custom Authenticator][2] call authenticate method
        authenticate(context);
    }

    @Override
    public void authenticate(AuthenticationFlowContext context) {
        UserModel userModel = context.getUser();

        String cid = userModel.getFirstAttribute(CID_ATTRIBUTE_NAME);

        /** [TODO][Custom Authenticator][3] check if cid Attribute is presented
         *
         * 1. retrieve user attribute using userModel.getFirstAttribute
         * 2. check if the attribute is exist
         * 3. use this code block to send error back to the user
         *
         *  context.getEvent().user(userModel).error(INVALID_CID_ERROR_MESSAGE);
         *  Response challengeResponse = context.form().setError(INVALID_CID_ERROR_MESSAGE).createLogin();
         *  context.failureChallenge(AuthenticationFlowError.INTERNAL_ERROR, challengeResponse);
         *  context.resetFlow();
         *
         * 4. user this code block to send success response if the user attribute is exist
         *  context.success();
         *
         */

        if (cid == null) {
            logger.errorf("Invalid User Attribute CID for user [%s] ", userModel.getUsername());

            context.getEvent().user(userModel).error(INVALID_CID_ERROR_MESSAGE);

            Response challengeResponse = context.form().setError(INVALID_CID_ERROR_MESSAGE).createLogin();
            context.failureChallenge(AuthenticationFlowError.INTERNAL_ERROR, challengeResponse);

            context.resetFlow();

            return;
        }

        context.success();
    }


    @Override
    public boolean requiresUser() {
        return true;
    }

    @Override
    public boolean configuredFor(KeycloakSession session, RealmModel realm, UserModel user) {
        return true;
    }

    @Override
    public void setRequiredActions(KeycloakSession session, RealmModel realm, UserModel user) {

    }

    @Override
    public void close() {

    }
}
